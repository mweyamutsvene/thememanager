//
//  THTheme.M
//
//
//  Created by Thomas Hanks.
//  Copyright (c) 2013 Thomas Hanks. All rights reserved.
//

#import "THTheme.h"

#import "THDefaultTheme.h"

@implementation THThemeManager
static id <THTheme> __sharedTheme;

+(void)createSharedThemeUsingClassName:(NSString*)theme;
{
    __sharedTheme = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Create and return the theme:
        __sharedTheme = [[NSClassFromString(theme) alloc] init];
    });
}

+ (id <THTheme>)sharedTheme
{
    if(!__sharedTheme){
        __sharedTheme = [[THDefaultTheme alloc] init];
    }
    return __sharedTheme;
}

+ (void)customizeAppAppearance
{
}

+ (void)customizeView:(UIView *)view
{
}

+ (void)customizePatternView:(UIView *)view
{
}

+ (void)customizeTimelineView:(UIView *)view
{
}

+ (void)customizeTableView:(UITableView *)tableView
{
}

+ (void)customizeTabBarItem:(UITabBarItem *)item forTab:(THThemeTab)tab
{
}

+ (void)customizeNavigationBar:(UINavigationBar *)navigationBar
{
}

+ (void)customizeMainLabel:(UILabel *)label
{
}

+ (void)customizeSecondaryLabel:(UILabel *)label
{
}

@end
