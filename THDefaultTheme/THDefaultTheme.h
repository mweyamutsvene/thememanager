//
//  ADVDefaultTheme.h
//  
//
//  Created by Valentin Filip on 7/9/12.
//  Copyright (c) 2013 AppDesignVault. All rights reserved.
//

#import "THTheme.h"

@interface THDefaultTheme : NSObject <THTheme>

@end
